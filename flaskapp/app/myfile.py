
from bs4 import BeautifulSoup
import requests
import re
import time
import mysql.connector
import urllib

url = 'http://www.meneame.net'

while True:

    #OBTENER DATOS DE LA PAGINA WEB

    response = requests.get(url)

    soup = BeautifulSoup(response.content,"html.parser")
    var_clics = soup.find_all("div", class_=re.compile("clics")) 
    var_votes = soup.find_all("div", class_=re.compile("votes"))
    var_title = soup.find_all("h2")
    
    clics = var_clics[5].text.split()   #solo el numero de clics
    print(clics[0])

    votes = var_votes[20].text.split()  #solo el numero de votes
    print(votes[0])

    title = var_title[0].text           #titulo
    print(title)
    
    ahora = time.strftime("%c")         #date
    print(ahora)

    #INTRODUCIR DATOS EN MYSQL 

    mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="PWD",
    database="mydatabase"
   )

    mycursor = mydb.cursor()

    sql = "INSERT INTO resultados (CLICS, VOTES, TITLE, FECHA) VALUES (%s, %s, %s, %s)"
    val = (int(clics[0]), int(votes[0]), title, ahora)
    mycursor.execute(sql, val)

    mydb.commit()
    mydb.close()

    print("1 record inserted in MYSQL, ID:", mycursor.lastrowid)


    #INTRODUCIR DATOS EN THINGSPEAK

    title_url = urllib.quote(title.encode('utf8'))
    url2 = "https://api.thingspeak.com/update?api_key=6XEK3XJQJI04J0JP&field1=%s&field2=%s&field3=%s&field4=%s" % (int(clics[0]), int(votes[0]), title_url, ahora)
    print(url2)
    
    data = urllib.urlopen(url2)

    time.sleep(120)


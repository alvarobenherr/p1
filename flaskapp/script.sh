	#SCRIPT

	#Configuration
	sudo apt-get install python-dev
	sudo apt-get install python-virtualenv
	mkdir clone
	cd clone/
	virtualenv flask
	. flask/bin/activate

	pip install flask
	pip install flask-script
	pip install git
	pip install python-pip
	pip install requests
	pip install beautifulsoup4
	pip install bs4
	pip install urllib
	pip install render_template

	apt-get install mysql server
	pip install mysql-connector

	#download flaskapp folder from gitlab
	git clone git@gitlab.com:alvarobenherr/p1.git

	#import database
	cd p1/flaskapp/
	mysql -u root -p'PWD' mydatabase < database-dump.sql

	#execute programs
	cd app/
	python routes.py &
	python myfile.py &





from flask import Flask, render_template ,make_response, request, redirect
import mysql.connector
import urllib
import re

flag = 1

app = Flask(__name__)


@app.route('/clics')
def grafica():
	return redirect("https://thingspeak.com/channels/623437/charts/1?bgcolor=%23000000&color=%23d62020&dynamic=true&results=100&type=line&update=15")

@app.route('/meneos')
def grafica2():
	return redirect("https://thingspeak.com/channels/623437/charts/2?bgcolor=%23000000&color=%23d62020&dynamic=true&results=100&type=line&update=15")

@app.route('/channel')
def canal():
	return redirect("https://thingspeak.com/channels/623437")


@app.route('/')
def home():
	
	global flag
	if flag:

		mydb = mysql.connector.connect(host="localhost",user="root",passwd="PWD",database="mydatabase")
		mycursor = mydb.cursor()

		sql = "SELECT AVG(CLICS) FROM resultados"
		mycursor.execute(sql)
		average = mycursor.fetchone()
		media = average[0]
		print(media)
		mydb.close()
		flag = 0
		return render_template('home.html', average = media, ddbb = "Mysql")

	else:

		datas = urllib.urlopen("https://api.thingspeak.com/channels/623437/feeds.json?results")
		select = repr(datas.read())
		select = select[260:]
		#print(select)

		pick = re.findall('field1":"(.+?)",',select)

		if pick:
		   print(pick)

		pick = list(map(int, pick))
		#print(pick)

		media = sum(pick) / float(len(pick))
		print(media)
		flag = 1
		return render_template('home.html', average = media, ddbb = "ThingSpeak")


@app.route('/consulta',methods=['POST'])
def consulta():

	limite = request.form['limit']
	print(limite)

	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	passwd="PWD",
	database="mydatabase"
   )

	mycursor = mydb.cursor()

	sql = "SELECT * FROM resultados where CLICS>%(limit)s ORDER BY FECHA DESC LIMIT 10"
	mycursor.execute(sql,{'limit':limite})

	data = mycursor.fetchall()
	print(data)
   
	mydb.close()

	global flag
	if flag:

		mydb = mysql.connector.connect(host="localhost",user="root",passwd="PWD",database="mydatabase")
		mycursor = mydb.cursor()

		sql = "SELECT AVG(CLICS) FROM resultados"
		mycursor.execute(sql)
		average = mycursor.fetchone()
		media = average[0]
		print(media)
		mydb.close()
		flag = 0
		return render_template('consulta.html', average = media, ddbb = "Mysql", limite=limite, data = data)

	else:

		datas = urllib.urlopen("https://api.thingspeak.com/channels/623437/feeds.json?results")
		select = repr(datas.read())
		select = select[260:]
		#print(select)

		pick = re.findall('field1":"(.+?)",',select)

		if pick:
		   print(pick)

		pick = list(map(int, pick))
		#print(pick)

		media = sum(pick) / float(len(pick))
		print(media)
		flag = 1
		return render_template('consulta.html', average = media, ddbb = "ThingSpeak", limite = limite, data = data)



   
if __name__ == '__main__':
	app.debug = False
	app.run(host='0.0.0.0',port=5000)

